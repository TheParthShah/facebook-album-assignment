<?php


if (!session_id()) {
    session_start();
}

require_once __DIR__.'/Facebook/autoload.php';

// Include required libraries
use Facebook\Facebook;

$appId = '1788939534522317'; //Facebook App ID
$appSecret = 'c097d5f9aa8809337c00ad7084f38fea'; //Facebook App Secret

$fb = new Facebook(array(
    'app_id' => $appId,
    'app_secret' => $appSecret,
    'default_graph_version' => 'v2.9',
        ));

// Get redirect login helper
$helper = $fb->getRedirectLoginHelper();
if (isset($_GET['state'])) {
    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
}

try {
    $accessToken = $helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: '.$e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: '.$e->getMessage();
    exit;
}

if (!isset($accessToken)) {
    if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'Error: '.$helper->getError()."\n";
        echo 'Error Code: '.$helper->getErrorCode()."\n";
        echo 'Error Reason: '.$helper->getErrorReason()."\n";
        echo 'Error Description: '.$helper->getErrorDescription()."\n";
    } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
    }
    exit;
}

$response = $fb->get('/me?fields=name,id,email,albums', $accessToken);
$user = $response->getGraphuser();

?>


    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>rtCamp Project</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    </head>
    <body>
        <div id="Container">
            <div class="UserProfileDisplay">
                <table class="HeaderTable" border="0px" cellpadding="10px">
                    <tr>
                        <td>
                            <?php
                                echo '&nbsp<img id="ProPic" src="https://graph.facebook.com/'.$user['id'].'/picture?return_ssl_resources=1" alt="Profile Picture of the user logged in" width="100px" height="100px" />';
                                echo '<h3 id="UserName"><p align="top">&nbspWelcome : '.$user['name'].'</p></h3>';
                                echo '<p id="UserEmail">ID : '.$user['email'].'</p></br>';
                            ?>
                        </td>                   
                    </tr>
                </table>            
            </div>    
            <br><br>

            <?php
                for ($i = 0; $i < count($user['albums']); ++$i) {
                    ?>
                    <div id="AlbumBlock">
                        <?php
                        echo '<h2><p id="AlbumNames">&nbsp'.$user['albums'][$i]['name'].'</p></h2>'; ?>
                        
                        <?php
                            $response = $fb->get('/'.$user['albums'][$i]['id'].'/photos?fields=id,name,images', $accessToken);
                    $photos = $response->getGraphEdge();
                    for ($j = 0; $j < count($photos); ++$j) {
                        echo '<img id="AlbumImages" src='.$photos[$j]['images'][2]['source'].' width="100" height="100">&nbsp';
                    } ?>

                        <p class="DriveButtonIndi">   
                            <?php echo '<a>Download Album!</a>'; ?>
                        </p>
                        <p class="DriveButtonIndi">   
                            <?php echo '<a>Upload This Album To Google Drive!</a>'; ?>
                        </p>
                    </div>
                    <br>
                    <?php
                }

            ?> 

        </div>
        <br>

         <!-- The Modal -->
        <div id="myModal" class="modal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
        </div>



        <footer id="footerCallback">
            <p id="footerName">Created By :<br> Parth Kinjal Shah</p>
            <p>Click To Mail:<br> 
                <a href="mailto:searchparth@gmail.com">searchparth@gmail.com</a><br>
                <a href="mailto:201712047@daiict.ac.in">201712047@daiict.ac.in</a>
            </p>
            <p id="footerNumber">Contact Number:<br> +91 940 902 7942</p> 
        </footer>



    <script>
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('AlbumImages');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() { 
        modal.style.display = "none";
    }
    </script>

                
    </body>
    </html>