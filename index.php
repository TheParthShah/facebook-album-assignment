
<?php


if (!session_id()) {
    session_start();
}

require_once __DIR__.'/Facebook/autoload.php';

// Include required libraries
use Facebook\Facebook;

$appId = '1788939534522317'; //Facebook App ID
$appSecret = 'c097d5f9aa8809337c00ad7084f38fea'; //Facebook App Secret

$fb = new Facebook(array(
    'app_id' => $appId,
    'app_secret' => $appSecret,
    'default_graph_version' => 'v2.9',
        ));

// Get redirect login helper
$helper = $fb->getRedirectLoginHelper();

$permissions = ['email,user_photos']; // Optional permissions
//$loginUrl = $helper->getLoginUrl('https://192.168.43.178/rtCampTrial4/fb-callback.php', $permissions);
//$loginUrl = $helper->getLoginUrl('https://parth.kinjal.in/fb-callback.php', $permissions);
$loginUrl = $helper->getLoginUrl('https://localhost/fb-callback.php', $permissions);

?>

<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>rtCamp Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    
</head>
<body>

    <div class="DisplayBlock">
        <h3 class="ProjectHeader">
            rtCamp Facebook Placement Project
        </h3>
        <p>   
            <?php echo '<a class="LinkButton" href="'.htmlspecialchars($loginUrl).'">Log in with Facebook!</a>'; ?>
        </p>
    </div>
    

    <footer id="footer">
        <p id="footerName">Created By :<br> Parth Kinjal Shah</p>
        <p>Click To Mail:<br> 
            <a href="mailto:searchparth@gmail.com">searchparth@gmail.com</a><br>
            <a href="mailto:201712047@daiict.ac.in">201712047@daiict.ac.in</a>
        </p>
        <p id="footerNumber">Contact Number:<br> +91 940 902 7942</p> 
    </footer>
</body>
</html>
